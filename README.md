# README #

This repository contains a version of the "narrowband" benchmark examples provided with GNU Radio, for use with a classroom [Spectrum Challenge](https://witestlab.poly.edu/blog/spectrum-challenge/). 

It has been modified so that the transmitter gets packets from a "packet source" server and the receiver delivers packets to a "packet sink".